{
    "group": {
        "hosts": [
            ${INVENTORY_HOSTS}
        ],
        "vars": {
          "ansible_python_interpreter": "/usr/bin/python3"
        }
    }
}
