variable "hcloud_token" {
  description = "The API token for Hetzner Cloud"
  type = string
}

variable "hcloud_location" {
  description = "The location to use for the infrastructure components"
  default = "nbg1"
}
